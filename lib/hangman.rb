

class Hangman
  attr_reader :guesser, :referee, :board

  def initialize(players)
    @guesser = players[:guesser]
    @referee = players[:referee]
  end

  def setup
    secret_word_length = @referee.pick_secret_word
    @guesser.register_secret_length(secret_word_length)
    set_board_to(secret_word_length)
  end

  def set_board_to(secret_word_length)
    @board = "_" * secret_word_length
  end

  def take_turn
    guessed_letter = @guesser.guess
    occurances_of_letter = @referee.check_guess(guessed_letter)
    update_board(occurances_of_letter, guessed_letter)
    @guesser.handle_response
  end

  def update_board(occurances_of_letter, guessed_letter)
    if !occurances_of_letter.empty?
      occurances_of_letter.each { |i| @board[i] = guessed_letter }
    end
  end

end

class HumanPlayer
  attr_accessor :secret_length, :secret_word, :guess

  def initialize(dictionary)
    @dictionary = dictionary
  end

  def pick_secret_word
    @dictionary.shuffle.first
    @secret_word = @dictionary.shuffle.first
    @secret_word.length
  end

  def register_secret_length(secret_length)
    @secret_length = secret_length
    @dictionary = self.candidate_words.select do |word|
      word if word.length == @secret_length
    end

  end

  def check_guess(letter)
    if correct_letter?(letter)
      occurances_of(letter)
    else []
    end
  end

  def correct_letter?(letter)
    @secret_word.include? letter
  end

  def occurances_of(letter)
    indicies = Array.new
    @secret_word.each_char.with_index do |ch, i|
      indicies << i if letter.downcase == ch.downcase
    end
    indicies
  end

  def guess(board)

    letters_sorted_by_freq = most_common_letter_in(candidate_words)
    current_guess = letters_sorted_by_freq.pop

    if board.include? current_guess
      until !(board.include? current_guess)
      current_guess = letters_sorted_by_freq.pop
      end
    end
    @guess = current_guess

  end

  def most_common_letter_in(candidate_words)
    letter_count = Hash.new(0)
    candidate_words.join.chars.each { |ch| letter_count[ch] += 1 }
    letter_count.sort_by { |k, v| v }.map(&:first)

  end

  def handle_response(*response)
    @dictionary = candidate_words.select do |word|
      word if letters_and_indicies_match?(word, response)
    end
  end

  def letters_and_indicies_match?(word, response)
    ref_indicies = response.last.sort
    letter = response.first
    act_indicies = Array.new
    word.chars.each_with_index do |ch, i|
      act_indicies << i if letter == ch
    end.sort
    ref_indicies == act_indicies
  end

  def candidate_words
    @dictionary
  end


end

class ComputerPlayer
  attr_accessor :secret_length, :secret_word, :guess

  def initialize(dictionary)
    @dictionary = dictionary
  end

  def pick_secret_word
    @dictionary.shuffle.first
    @secret_word = @dictionary.shuffle.first
    @secret_word.length
  end

  def register_secret_length(secret_length)
    @secret_length = secret_length
    @dictionary = self.candidate_words.select do |word|
      word if word.length == @secret_length
    end

  end

  def check_guess(letter)
    if correct_letter?(letter)
      occurances_of(letter)
    else []
    end
  end

  def correct_letter?(letter)
    @secret_word.include? letter
  end

  def occurances_of(letter)
    indicies = Array.new
    @secret_word.each_char.with_index do |ch, i|
      indicies << i if letter.downcase == ch.downcase
    end
    indicies
  end

  def guess(board)

    letters_sorted_by_freq = most_common_letter_in(candidate_words)
    current_guess = letters_sorted_by_freq.pop

    if board.include? current_guess
      until !(board.include? current_guess)
      current_guess = letters_sorted_by_freq.pop
      end
    end
    @guess = current_guess

  end

  def most_common_letter_in(candidate_words)
    letter_count = Hash.new(0)
    candidate_words.join.chars.each { |ch| letter_count[ch] += 1 }
    letter_count.sort_by { |k, v| v }.map(&:first)

  end

  def handle_response(*response)
    @dictionary = candidate_words.select do |word|
      word if letters_and_indicies_match?(word, response)
    end
  end

  def letters_and_indicies_match?(word, response)
    ref_indicies = response.last.sort
    letter = response.first
    act_indicies = Array.new
    word.chars.each_with_index do |ch, i|
      act_indicies << i if letter == ch
    end.sort
    ref_indicies == act_indicies
  end

  def candidate_words
    @dictionary
  end


end
